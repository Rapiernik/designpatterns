package com.patterns.structural.bridge;

/**
 * https://www.baeldung.com/java-bridge-pattern
 * <p>
 * This design pattern is to decouple an abstraction from its implementation so that
 * the two can vary independently.
 * <p>
 * This means to create a bridge interface that uses OOP principles to separate out responsibilities into different abstract classes.
 */
public class BridgeMain {

    public static void main(String[] args) {
        Color blueColor = new BlueColor();
        Shape squareShape = new SquareShape(blueColor);

        System.out.println(squareShape.draw());
    }

}
