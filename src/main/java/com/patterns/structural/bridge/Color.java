package com.patterns.structural.bridge;

public interface Color {

    String fill();
}
