package com.patterns.structural.adapter;

public interface Movable {
    // returns speed in MPH
    double getSpeed();
}
