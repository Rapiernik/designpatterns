package com.patterns.structural.adapter;

/**
 * https://www.baeldung.com/java-adapter-pattern
 * <p>
 * An Adapter pattern acts as a connector between two incompatible interfaces that otherwise cannot be connected directly
 */
public class AdapterMain {

    public static void main(String[] args) {
        Movable car = new BugattiVeyron();
        MovableAdapter adapter = new MovableAdapterImpl(car);

        System.out.println(adapter.getSpeed());
    }

}
