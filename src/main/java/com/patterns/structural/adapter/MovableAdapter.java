package com.patterns.structural.adapter;

public interface MovableAdapter {
    // returns speed in KM/H
    double getSpeed();
}
