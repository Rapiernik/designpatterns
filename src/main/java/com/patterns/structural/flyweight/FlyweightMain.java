package com.patterns.structural.flyweight;

/**
 * https://www.baeldung.com/java-flyweight
 * <p>
 * Simply put, the flyweight pattern is based on a factory which recycles created objects by storing them after creation. Each time an object is requested, the factory looks up the object in order to check if it's already been created. If it has, the existing object is returned – otherwise, a new one is created, stored and then returned.
 */
public class FlyweightMain {

    public static void main(String[] args) {

        Vehicle blueCar = VehicleFactory.createVehicle("blue");
        System.out.println(blueCar);

        Vehicle redCar = VehicleFactory.createVehicle("red");
        System.out.println(redCar);

        Vehicle theSameBlueCar = VehicleFactory.createVehicle("blue");
        System.out.println(theSameBlueCar);

        Vehicle theSameRedCar = VehicleFactory.createVehicle("red");
        System.out.println(redCar);
    }

}
