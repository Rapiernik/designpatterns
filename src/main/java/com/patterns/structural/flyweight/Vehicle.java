package com.patterns.structural.flyweight;

public interface Vehicle {

    void start();

    void stop();

    String getColor();

}
