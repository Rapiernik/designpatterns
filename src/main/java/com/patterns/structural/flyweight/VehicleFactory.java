package com.patterns.structural.flyweight;

import java.util.HashMap;
import java.util.Map;

public class VehicleFactory {

    private static Map<String, Vehicle> vehiclesCache = new HashMap<>();

    public static Vehicle createVehicle(String color) {

        return vehiclesCache.computeIfAbsent(color, newColor -> {
            String newEngine = "newEngine";
            return new Car(newEngine, newColor);
        });

    }
}
