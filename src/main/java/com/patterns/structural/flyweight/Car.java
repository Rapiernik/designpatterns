package com.patterns.structural.flyweight;

public class Car implements Vehicle {

    private final String engine;
    private final String color;

    public Car(String engine, String color) {
        this.engine = engine;
        this.color = color;
    }

    @Override
    public void start() {
        System.out.println("starting car");
    }

    @Override
    public void stop() {
        System.out.println("stopping car");
    }

    @Override
    public String getColor() {
        return color;
    }

}
