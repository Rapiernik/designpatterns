package com.patterns.structural.proxy;

public class ExpensiveObjectImpl implements ExpensiveObject {

    public ExpensiveObjectImpl() {
        heavyInitialConfiguration();
    }

    private void heavyInitialConfiguration() {
        System.out.println("Loading heavy initial configuration...");
    }

    public void process() {
        System.out.println("processing staff...");
    }
}
