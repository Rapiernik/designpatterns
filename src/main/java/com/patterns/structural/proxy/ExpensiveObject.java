package com.patterns.structural.proxy;

public interface ExpensiveObject {

    void process();

}
