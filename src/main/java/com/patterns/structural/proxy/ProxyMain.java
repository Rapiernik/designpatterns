package com.patterns.structural.proxy;

/**
 * https://www.baeldung.com/java-proxy-pattern
 * <p>
 * The Proxy pattern allows us to create an intermediary that acts as an interface to another resource, while also hiding the underlying complexity of the component.
 */
public class ProxyMain {

    public static void main(String[] args) {
        ExpensiveObject expensiveObject = new ExpensiveObjectProxy();

        expensiveObject.process();
        expensiveObject.process();
        expensiveObject.process();
        expensiveObject.process();
    }
}
