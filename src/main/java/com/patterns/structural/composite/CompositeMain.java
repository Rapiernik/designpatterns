package com.patterns.structural.composite;

/**
 * https://www.baeldung.com/java-composite-pattern
 * <p>
 * The composite pattern is meant to allow treating individual objects and compositions of objects, or “composites” in the same way.
 * <p>
 * It can be viewed as a tree structure made up of types that inherit a base type, and it can represent a single part or a whole hierarchy of objects.
 */
public class CompositeMain {

    public static void main(String[] args) {

        Department financialDepartment = new FinancialDepartment(1, "Financial department");
        Department salesDepartment = new SalesDepartment(2, "Sales department");

        HeadDepartment headDepartment = new HeadDepartment(3, "Head department");
        headDepartment.addDepartment(financialDepartment);
        headDepartment.addDepartment(salesDepartment);

        headDepartment.printDepartmentName();
    }

}
