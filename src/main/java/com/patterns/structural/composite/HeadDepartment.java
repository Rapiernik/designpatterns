package com.patterns.structural.composite;

import java.util.ArrayList;
import java.util.List;

public class HeadDepartment implements Department {

    private final Integer id;
    private final String name;
    private final List<Department> departments;

    public HeadDepartment(Integer id, String name) {
        this.id = id;
        this.name = name;
        this.departments = new ArrayList<>();
    }

    public void printDepartmentName() {
        departments.forEach(Department::printDepartmentName);
    }

    public void addDepartment(Department department) {
        departments.add(department);
    }

    public void removeDepartment(Department department) {
        departments.remove(department);
    }
}
