package com.patterns.structural.facade;

public class OrderFacade {

    private Waiter waiter = new Waiter();
//    private Waiter waiter;
    private Kitchen kitchen = new Kitchen();
//    private Kitchen kitchen;

    public void orderFood() {
        waiter.takeOrder();
        waiter.sendOrderToCook();
        kitchen.cookFood();
        kitchen.signalReady();
        waiter.serveFoodToClient();
    }
}
