package com.patterns.structural.facade;

public class Waiter {

    public void takeOrder() {
        System.out.println("Take order from client");
    }

    public void sendOrderToCook() {
        System.out.println("Send order to cook");
    }

    public void serveFoodToClient() {
        System.out.println("Serve food to client");
    }
}
