package com.patterns.structural.facade;

/**
 * https://www.baeldung.com/java-facade-pattern
 * <p>
 * Simply put, a facade encapsulates a complex subsystem behind a simple interface. It hides much of the complexity and makes the subsystem easy to use.
 */
public class FacadeMain {

    public static void main(String[] args) {

        OrderFacade orderFacade = new OrderFacade();
        orderFacade.orderFood();

    }

}
