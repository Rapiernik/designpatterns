package com.patterns.structural.decorator;

/**
 * https://www.baeldung.com/java-decorator-pattern
 * <p>
 * A Decorator pattern can be used to attach additional responsibilities to an object either statically or dynamically
 */
public class DecoratorMain {

    public static void main(String[] args) {
        ChristmasTree christmasTree = new Tinsel(new BubbleLights(new Tinsel(new ChristmasTreeImpl())));
        System.out.println(christmasTree.decorate());
    }
}
