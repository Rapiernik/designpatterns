package com.patterns.structural.decorator;

public interface ChristmasTree {

    String decorate();
}
