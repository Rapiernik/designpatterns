package com.patterns.structural.decorator;

public class Tinsel extends TreeDecorator {

    public Tinsel(ChristmasTree christmasTree) {
        super(christmasTree);
    }

    public String decorate() {
        return super.decorate() + decorateWithTinsel();
    }

    private String decorateWithTinsel() {
        return " with tinsel";
    }
}
