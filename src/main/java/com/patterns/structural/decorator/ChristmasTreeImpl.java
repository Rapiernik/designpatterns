package com.patterns.structural.decorator;

public class ChristmasTreeImpl implements ChristmasTree {

    @Override
    public String decorate() {
        return "Decorating christmas tree";
    }
}
