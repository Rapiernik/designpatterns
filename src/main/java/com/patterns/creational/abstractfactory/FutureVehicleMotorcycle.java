package com.patterns.creational.abstractfactory;

import com.patterns.creational.factory.MotorVehicle;

public class FutureVehicleMotorcycle implements MotorVehicle {

    @Override
    public void build() {
        System.out.println("Future Vehicle Motorcycle");
    }
}
