package com.patterns.creational.abstractfactory;

public interface ElectricVehicle {

    void build();
}
