package com.patterns.creational.abstractfactory;

import com.patterns.creational.factory.MotorVehicle;

/**
 * https://www.baeldung.com/java-factory-pattern
 * <p>
 * This pattern is commonly used when we start using the Factory Method Pattern, and we need to evolve our system to a more complex system. It centralizes the product creation code in one place
 * <p>
 * The Factory Method uses inheritance as a design tool. Meanwhile, Abstract Factory uses delegation.
 */
public class AbstractFactoryMain {

    public static void main(String[] args) {
        Corporation futureCorporation = new FutureVehicleCorporation();

        ElectricVehicle futureElectricVehicle = futureCorporation.createElectricVehicle();
        System.out.println(futureElectricVehicle);

        MotorVehicle futureMotorVehicle = futureCorporation.createMotorVehicle();
        System.out.println(futureMotorVehicle);


        Corporation nextGenCorporation = new NextGenCorporation();

        ElectricVehicle nextGenElectricVehicle = nextGenCorporation.createElectricVehicle();
        System.out.println(nextGenElectricVehicle);

        MotorVehicle nextGenMotorVehicle = nextGenCorporation.createMotorVehicle();
        System.out.println(nextGenMotorVehicle);
    }

}
