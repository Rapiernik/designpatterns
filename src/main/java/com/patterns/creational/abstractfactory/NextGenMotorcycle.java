package com.patterns.creational.abstractfactory;

import com.patterns.creational.factory.MotorVehicle;

public class NextGenMotorcycle implements MotorVehicle {

    @Override
    public void build() {
        System.out.println("NextGen Motorcycle");
    }

}
