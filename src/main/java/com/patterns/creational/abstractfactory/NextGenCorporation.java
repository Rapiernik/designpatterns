package com.patterns.creational.abstractfactory;

import com.patterns.creational.factory.MotorVehicle;

public class NextGenCorporation extends Corporation {

    @Override
    public MotorVehicle createMotorVehicle() {
        return new NextGenMotorcycle();
    }

    @Override
    public ElectricVehicle createElectricVehicle() {
        return new NextGenElectricCar();
    }

}
