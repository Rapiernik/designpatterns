package com.patterns.creational.abstractfactory;

import com.patterns.creational.factory.MotorVehicle;

public class FutureVehicleCorporation extends Corporation {

    @Override
    public MotorVehicle createMotorVehicle() {
        return new FutureVehicleMotorcycle();
    }

    @Override
    public ElectricVehicle createElectricVehicle() {
        return new FutureVehicleElectricCar();
    }

}
