package com.patterns.creational.abstractfactory;

import com.patterns.creational.factory.MotorVehicle;

public abstract class Corporation {

    public abstract MotorVehicle createMotorVehicle();

    public abstract ElectricVehicle createElectricVehicle();
}
