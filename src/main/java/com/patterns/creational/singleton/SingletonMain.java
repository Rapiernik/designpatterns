package com.patterns.creational.singleton;

/**
 * https://www.baeldung.com/java-singleton
 * <p>
 * The Singleton Design Pattern aims to keep a check on initialization of objects of a particular class by ensuring that only one instance of the object exists throughout the Java Virtual Machine.
 */
public class SingletonMain {

    public static void main(String[] args) {


        System.out.println(SimpleThreadSafeSingleton.getInstance());
        System.out.println(SimpleThreadSafeSingleton.getInstance());
        System.out.println(SimpleThreadSafeSingleton.getInstance());
    }

}
