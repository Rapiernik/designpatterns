package com.patterns.creational.singleton;

public class SimpleThreadSafeSingleton {

    private static volatile SimpleThreadSafeSingleton instance = null;

    private SimpleThreadSafeSingleton() {
        if (instance != null) {
            throw new RuntimeException("Singleton instance already exists!");
        }
    }

    public static SimpleThreadSafeSingleton getInstance() {
        if (instance == null) {
            synchronized (SimpleThreadSafeSingleton.class) {
                if (instance == null) {
                    instance = new SimpleThreadSafeSingleton();
                }
            }
        }

        return instance;
    }
}
