package com.patterns.creational.factory;

/**
 * https://www.baeldung.com/java-factory-pattern
 * <p>
 * The factory design pattern is used when we have a superclass with multiple sub-classes and based on input, we need to return one of the sub-class. This pattern takes out the responsibility of the instantiation of a class from the client program to the factory class
 */
public class FactoryMain {

    public static void main(String[] args) {

        MotorVehicleFactory carFactory = new CarFactory();
        MotorVehicle car = carFactory.create();
        System.out.println(car);

        MotorVehicleFactory motorcycleFactory = new MotorcycleFactory();
        MotorVehicle motorcycle = motorcycleFactory.create();
        System.out.println(motorcycle);

    }

}
