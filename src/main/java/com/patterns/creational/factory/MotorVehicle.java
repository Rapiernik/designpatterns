package com.patterns.creational.factory;

public interface MotorVehicle {

    void build();
}
