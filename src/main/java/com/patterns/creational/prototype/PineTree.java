package com.patterns.creational.prototype;

public class PineTree extends Tree {

    public PineTree(Integer mass, Integer height) {
        this.mass = mass;
        this.height = height;
    }

    public Integer getMass() {
        return mass;
    }

    public Integer getHeight() {
        return height;
    }

    public String getPosition() {
        return position;
    }

    @Override
    public Tree copy() {
        PineTree pineTree = new PineTree(this.getMass(), this.getHeight());
        pineTree.setPosition(this.getPosition());
        return pineTree;
    }
}
