package com.patterns.creational.prototype;

public abstract class Tree {

    protected Integer mass;
    protected Integer height;
    protected String position;

    public abstract Tree copy();

    public void setPosition(String position) {
        this.position = position;
    }
}
