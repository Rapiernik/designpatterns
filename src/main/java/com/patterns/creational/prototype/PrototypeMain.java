package com.patterns.creational.prototype;

/**
 * https://www.baeldung.com/java-pattern-prototype
 * <p>
 * The Prototype pattern is generally used when we have an instance of the class (prototype) and we'd like to create new objects by just copying the prototype.
 */
public class PrototypeMain {

    public static void main(String[] args) {

        Tree plasticTree = new PlasticTree(1, 24);
        plasticTree.setPosition("15");

        System.out.println(plasticTree);
        System.out.println(plasticTree.copy());
        System.out.println(plasticTree.copy());
    }

}
