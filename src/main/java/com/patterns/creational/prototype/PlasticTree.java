package com.patterns.creational.prototype;

public class PlasticTree extends Tree {

    public PlasticTree(Integer mass, Integer height) {
        this.mass = mass;
        this.height = height;
    }

    public Integer getMass() {
        return mass;
    }

    public Integer getHeight() {
        return height;
    }

    public String getPosition() {
        return position;
    }

    @Override
    public Tree copy() {
        PlasticTree plasticTreeClone = new PlasticTree(this.getMass(), this.getHeight());
        plasticTreeClone.setPosition(this.getPosition());
        return plasticTreeClone;
    }
}
