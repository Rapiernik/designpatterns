package com.patterns.creational.builder;

/**
 * https://www.baeldung.com/creational-design-patterns
 * <p>
 * When the complexity of creating object increases, the Builder pattern can separate out the instantiation process by using another object (a builder) to construct the object.
 */
public class BuilderMain {

    public static void main(String[] args) {

        BankAccount newAccount = new BankAccount
                .BankAccountBuilder("Jon", "22738022275")
                .withEmail("jon@example.com")
                .wantNewsletter(true)
                .build();

        System.out.println(newAccount);

    }

}
